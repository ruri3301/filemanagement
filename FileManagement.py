import os
import shutil
import time
import logging
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

source_dir = "D:\Downloads\Downloads 2022" #Source Download Folder 

dest_dir_sfx ="" #Good for Manga/ Video Editing
dest_dir_music = "D:\Managed Downloads\Music" #Target Music Folder
dest_dir_video = "D:\Managed Downloads\Video" #Target Video Folder
dest_dir_image = "D:\Managed Downloads\Image" #Target Image Folder
dest_dir_pdf = "D:\Managed Downloads\PDF" #Target Document, PDF Folder

def makeUnique(path):
    filename, extension = os.path.splitext(path)
    counter = 1
        # If Ever File Name Exist it will add a number EG. Oten1.jpg #MAKABOGOPISTE #YAWA #PISTE
    while os.path.exists(path):
        path = filename + " (" + str(counter) + ")" + extension
        counter += 1

    return path

def move(dest, entry, name):
    file_exists = os.path.exists(dest + "/" + name)
    if file_exists:
        unique_name = makeUnique(name)
        os.rename(entry, unique_name)
    shutil.move(entry,dest) 

class MoveFiles(FileSystemEventHandler):
    # This Function will only run if there a slight change/new Files on Source Download Folder"
    def on_modified(self, event):
        with os.scandir(source_dir) as entries:
            for entry in entries:
                name = entry.name
                dest = source_dir
                
                if name.endswith('.wav') or name.endswith('.mp3'):
                    if entry.stat().st_size < 25000000 or "SFX" in name:
                        dest = dest_dir_sfx
                    else:
                        dest = dest_dir_music
                    move(dest, entry, name)
                elif name.endswith('.mov') or name.endswith('.mp4'): #add more for diff types of file name
                    dest = dest_dir_video
                    move(dest, entry, name)
                elif name.endswith('.jpg') or name.endswith('.jpeg') or name.endswith('.png') or name.endswith('.PNG'):  # Some images png are in uppercase
                    dest = dest_dir_image
                    move(dest, entry, name)                   
                elif name.endswith('.pdf'):
                    dest = dest_dir_pdf
                    move(dest, entry, name)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    path = source_dir
    event_handler = MoveFiles()
    observer = Observer()
    observer.schedule(event_handler, path, recursive=True)
    observer.start()
    try:
        while True:
            time.sleep(10)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()